import { LoginAPI } from "../../components/Login/LoginAPI";
import {
  ACTION_LOGIN_ATTEMPTING,
  ACTION_LOGIN_SUCCESS,
  loginErrorAction,
  loginSuccessAction,
} from "../actions/loginActions";
import { sessionSetAction } from "../actions/sessionAction";

export const loginMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);

    if (action.type === ACTION_LOGIN_ATTEMPTING) {
      // MAKE HTTP REQUAST TO TRY AND LOGIN
      LoginAPI.login(action.payload)
        .then((profile) => {
          //login success ->
          dispatch(loginSuccessAction(profile));
        })
        .catch((error) => {
          //error ->
          dispatch(loginErrorAction(error.message));
        });
    }

    if (action.type === ACTION_LOGIN_SUCCESS) {
      // LOG ME IN!
      dispatch(sessionSetAction(action.payload));
    }
  };
