import { ACTION_SESSION_SET } from "../actions/sessionAction";

const initialState = {
  avatar: "",
  createdAt: "",
  id: "",
  lastLogin: "",
  token: "",
  username: "",
  loggedin: false,
};

export const sessionReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_SESSION_SET:
      return {
        ...action.payload,
        loggedin: true,
      };
    default:
      return state;
  }
};
