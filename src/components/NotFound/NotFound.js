import { Link } from "react-router-dom";

const NotFound = () => {
  return (
    <main>
      <h4>Hey you seem lost.</h4>
      <p>This page does not Exist!</p>
      <Link to="/">Take me Home</Link>
    </main>
  );
};

export default NotFound;
