import "./App.css";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Login from "./components/Login/Login";
import NotFound from "./components/NotFound/NotFound";
import AppContainer from "./hoc/AppContainer";
import Translator from "./components/Translator/Translator";

function App() {
  return (
    <BrowserRouter>
      <AppContainer>
        <div className="App">
          <h1>React Translator App</h1>
          <h4>
            <span className="material-icons">home</span>
          </h4>

          <Switch>
            <Route path="/" exact component={Login}></Route>
            <Route path="/translator" component={Translator}></Route>
            <Route path="*" component={NotFound}></Route>
          </Switch>
        </div>
      </AppContainer>
    </BrowserRouter>
  );
}

export default App;
